const path = require('path');
const Celonaut = require('../models/celonauts');

exports.index = function (req, res) {
    res.sendFile(path.resolve('views/celonauts.html'));
};

exports.create = function (req, res) {
    var newCelonaut = new Celonaut(req.body);
    console.log(req.body);
    newCelonaut.save(function (err) {
            if(err) {
            res.status(400).send('Unable to save celonaut to database');
        } else {
            res.redirect('/celonauts/getcelonaut');
        }
  });
               };

exports.list = function (req, res) {
        Celonaut.find({}).exec(function (err, celonauts) {
                if (err) {
                        return res.send(500, err);
                }
                res.render('getcelonaut', {
                        celonauts: celonauts
             });
        });
};
