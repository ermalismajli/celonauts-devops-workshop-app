const express = require('express');
const router = express.Router();
const celonaut = require('../controllers/celonauts');

router.get('/', function(req, res){
    celonaut.index(req,res);
});

router.post('/addcelonaut', function(req, res) {
    celonaut.create(req,res);
});

router.get('/getcelonaut', function(req, res) {
    celonaut.list(req,res);
});

module.exports = router;
